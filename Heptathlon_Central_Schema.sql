create table Famille
(
    ID  int auto_increment
        primary key,
    Nom varchar(255) not null
);

create table Magasins
(
    ID          int auto_increment,
    Nom         varchar(255) not null,
    Coordonnees varchar(255) null,
    constraint Magasins_ID_uindex
        unique (ID)
);

alter table Magasins
    add primary key (ID);

create table Facture
(
    ID           int auto_increment,
    ModePaiement varchar(255) null,
    DateFacture  varchar(255) null,
    Total        float        null,
    magasin_ID   int          not null,
    constraint Facture_ID_uindex
        unique (ID),
    constraint Facture_Magasins_ID_fk
        foreign key (magasin_ID) references Magasins (ID)
            on update cascade on delete cascade
);

alter table Facture
    add primary key (ID);

create table Produits
(
    ID        int auto_increment,
    Reference varchar(255) not null,
    Prix      float        null,
    FamilleID int          null,
    constraint Produit_Référence_uindex
        unique (Reference),
    constraint Produits_ID_uindex
        unique (ID),
    constraint Produit_Famille_ID_fk
        foreign key (FamilleID) references Famille (ID)
            on update cascade on delete cascade
);

alter table Produits
    add primary key (ID);

create table Facture_Produits
(
    ID          int auto_increment,
    facture_ID  int null,
    produits_ID int null,
    constraint Facture_Produits_ID_uindex
        unique (ID),
    constraint Facture_Produits_Produits_ID_fk
        foreign key (produits_ID) references Produits (ID)
            on update set null on delete set null
);

create index Facture_Produits_Facture_ID_fk
    on Facture_Produits (facture_ID);

alter table Facture_Produits
    add primary key (ID);

create table Magasins_Produits
(
    ID          int auto_increment,
    magasins_ID int null,
    produits_ID int null,
    quantite    int null,
    constraint Magasins_Produits_ID_uindex
        unique (ID),
    constraint Magasins_Produits_Magasins_ID_fk
        foreign key (magasins_ID) references Magasins (ID),
    constraint Magasins_Produits_Produits_ID_fk
        foreign key (produits_ID) references Produits (ID)
);

alter table Magasins_Produits
    add primary key (ID);


