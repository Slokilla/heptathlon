package org.heptathlon.rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public final class Client {
    public static void main(String[] args)
            throws RemoteException, NotBoundException, MalformedURLException {
        // L'interface est nécéssaire pour caster ici
        HelloService service = (HelloService) Naming.lookup("rmi://localhost:5099/server_33");
        System.out.println(service.sum(5, 10));
    }
}
