package org.heptathlon.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface HelloService extends Remote {
    int sum(int a, int b) throws RemoteException;
}
