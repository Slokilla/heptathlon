package org.heptathlon.serveurCentral;

import org.heptathlon.serveurCentral.entity.Facture;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement
@XmlSeeAlso(Facture.class)
public class Changements {

    private static Changements instance;
    private List<Facture> nvlFactures;

    private Changements() {
    }

    public static Changements getInstance() {
        if (instance == null)
            instance = new Changements();
        return instance;
    }

    public List<Facture> getNvlFactures() {
        return nvlFactures;
    }

    public void add(Facture f){
        nvlFactures.add(f);
    }
}
