package org.heptathlon.serveurCentral.Exceptions;

public class BadDateFormatException extends Exception {
    public BadDateFormatException(String s) {
        super(s);
    }

    public BadDateFormatException() {
        super();
    }

    public BadDateFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
