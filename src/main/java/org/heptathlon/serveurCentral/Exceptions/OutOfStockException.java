package org.heptathlon.serveurCentral.Exceptions;

public class OutOfStockException extends Exception {

    public OutOfStockException() {
        super();
    }

    public OutOfStockException(String message) {
        super(message);
    }

    public OutOfStockException(String message, Throwable cause) {
        super(message, cause);
    }
}
