package org.heptathlon.serveurCentral;

import org.heptathlon.serveurCentral.operation.CentralService;
import org.heptathlon.serveurCentral.operation.Operations;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Main {

    public static void main(String[] args) throws RemoteException {

        System.out.println("Lancement du serveur");

        try {
            // Create an instance of the server object
            Operations ope = Operations.getInstance();
            System.out.println("Publishing Server...");
            Registry result = LocateRegistry.createRegistry(2099);
            Naming.rebind(CentralService.LOOKUPNAME, ope);
            System.out.println("Server is Listening");


        } catch (Exception e) {
            System.err.println("Oula ça pue");
            e.printStackTrace();
            System.exit(1);
        }

    }

}