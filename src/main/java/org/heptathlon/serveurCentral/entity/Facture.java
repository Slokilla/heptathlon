package org.heptathlon.serveurCentral.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Facture", schema = "decathlon")
public class Facture {
    private int id;
    private String modePaiement;
    private String dateFacture;
    private Double total;
    private List<Produits> produits = new ArrayList<Produits>();
    private Magasins magasin;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ModePaiement")
    public String getModePaiement() {
        return modePaiement.toString();
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }

    @Basic
    @Column(name = "DateFacture")
    public String getDateFacture() {
        return dateFacture;
    }

    public void setDateFacture(String dateFacture) {
        this.dateFacture = dateFacture;
    }

    @Basic
    @Column(name = "Total")
    public Double getTotal() {
        return total;
    }

    public void calculateTotal(){
        this.total = 0d;
        for (Produits p:produits) {
            this.total += p.getPrix();
        }
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * Gestion de la relation Facture Produit
     */
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<Produits> getProduits() {
        return produits;
    }

    public void setProduits(List<Produits> produits) {
        this.produits = produits;
    }

    public void addProduit(Produits produit){
        this.produits.add(produit);
    }

    public void removeProduit(Produits produit){
        this.produits.remove(produit);
    }

    @ManyToOne
    public Magasins getMagasin(){
        return magasin;
    }

    public void setMagasin(Magasins magasin){
        this.magasin = magasin;
    }


    ////////Constructeurs////////
    public Facture() {
    }


    ////////OVERRRIDES/////////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Facture that = (Facture) o;

        if (id != that.id) return false;
        if (!Objects.equals(modePaiement, that.modePaiement)) return false;
        if (!Objects.equals(dateFacture, that.dateFacture)) return false;
        return Objects.equals(total, that.total);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (modePaiement != null ? modePaiement.hashCode() : 0);
        result = 31 * result + (dateFacture != null ? dateFacture.hashCode() : 0);
        result = 31 * result + (total != null ? total.hashCode() : 0);
        return result;
    }

    public static enum ModePaiement{
        CB ("CB"),
        Cheque ("Cheque"),
        Espece ("Espece");

        private String mode = "";

        ModePaiement(String name){
            this.mode = name;
        }

        public String getMode(){
            return mode;
        }
    }
}
