package org.heptathlon.serveurCentral.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Magasins", schema = "decathlon")
public class Magasins {
    private int id;
    private String nom;
    private String coordonnees;
    private List<Produits> produits = new ArrayList<Produits>();
    private List<Facture> factures = new ArrayList<Facture>();

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "Coordonnees")
    public String getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(String coordonnees) {
        this.coordonnees = coordonnees;
    }

    /**
     * Gestion de la relation Magasin --> Produit
     */
    @ManyToMany()
    public List<Produits> getProduits() {
        return produits;
    }

    public void setProduits(List<Produits> produits) {
        this.produits = produits;
    }

    public void addProduit(Produits produit){
        this.produits.add(produit);
    }

    public void removeProduit(Produits produit){
        this.produits.remove(produit);
    }

    /**
     * Gestion de la relation Magasin --> facture
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Facture> getFactures(){
        return factures;
    }

    private void setFactures(List<Facture> factures){
        this.factures = factures;
    }

    public void addFacture(Facture facture){
        this.factures.add(facture);
    }

    public void removeFacture(Facture facture){
        this.factures.remove(facture);
    }

    ////////Constructors/////////

    public Magasins() {
    }

    public Magasins(String nom, String coordonnees) {
        this.nom = nom;
        this.coordonnees = coordonnees;
    }


    ///////OVERRIDES////////
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Magasins that = (Magasins) o;

        if (id != that.id) return false;
        if (!Objects.equals(nom, that.nom)) return false;
        if (!Objects.equals(coordonnees, that.coordonnees)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (coordonnees != null ? coordonnees.hashCode() : 0);
        return result;
    }
}
