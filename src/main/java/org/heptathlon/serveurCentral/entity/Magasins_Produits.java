package org.heptathlon.serveurCentral.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Magasins_Produits", schema = "decathlon")
public class Magasins_Produits {
    private int id;
    private Integer magasinsId;
    private Integer produitsId;
    private Integer quantite; //@TODO parametrer le getter poru ne aps passer en dessous de 0

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "magasins_ID")
    public Integer getMagasinsId() {
        return magasinsId;
    }

    public void setMagasinsId(Integer magasinsId) {
        this.magasinsId = magasinsId;
    }

    @Basic
    @Column(name = "produits_ID")
    public Integer getProduitsId() {
        return produitsId;
    }

    public void setProduitsId(Integer produitsId) {
        this.produitsId = produitsId;
    }

    @Basic
    @Column(name = "quantite")
    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Magasins_Produits that = (Magasins_Produits) o;

        if (id != that.id) return false;
        if (!Objects.equals(magasinsId, that.magasinsId)) return false;
        if (!Objects.equals(produitsId, that.produitsId)) return false;
        if (!Objects.equals(quantite, that.quantite)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (magasinsId != null ? magasinsId.hashCode() : 0);
        result = 31 * result + (produitsId != null ? produitsId.hashCode() : 0);
        result = 31 * result + (quantite != null ? quantite.hashCode() : 0);
        return result;
    }
}
