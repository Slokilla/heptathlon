package org.heptathlon.serveurCentral.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Produits", schema = "decathlon")
public class Produits {
    private int id;
    private String reference;
    private Double prix;
    private Famille famille;
    private List<Magasins> magasins = new ArrayList<>();



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Basic
    @Column(name = "Prix")
    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }


    /**
     * Gestion de la relation Produit --> Famille
     */
    @ManyToOne
    @JoinColumn(name="FamilleID",
                foreignKey = @ForeignKey(name = "Produit_Famille_ID_fk")
    )
    public Famille getFamille() {
        return famille;
    }

    public void setFamille(Famille famille) {
        this.famille = famille;
    }

    /**
     * Gestion de la relation Produit --> Magasin
     */
    @ManyToMany(targetEntity = Magasins.class,mappedBy = "produits")
    public List<Magasins> getMagasins() {
        return magasins;
    }

    public void setMagasins(List<Magasins> magasins) {
        this.magasins = magasins;
    }

    public void addMagasin(Magasins magasin){
        this.magasins.add(magasin);
    }

    public void removeMagasin(Magasins magasin){
        this.magasins.remove(magasin);
    }

    ///////CONSTRUCTORS////////


    public Produits(String reference, Double prix) {
        this.reference = reference;
        this.prix = prix;
    }

    public Produits() {
    }


    ///////SURCHARGES/////////

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Produits that = (Produits) o;

        if (id != that.id) return false;
        if (!Objects.equals(reference, that.reference)) return false;
        return Objects.equals(prix, that.prix);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (reference != null ? reference.hashCode() : 0);
        result = 31 * result + (prix != null ? prix.hashCode() : 0);
        return result;
    }
}
