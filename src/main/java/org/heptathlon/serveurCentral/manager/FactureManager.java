package org.heptathlon.serveurCentral.manager;

import org.heptathlon.serveurCentral.entity.Facture;

import java.util.List;

public class FactureManager extends TableOperations<Facture> {

    private static FactureManager instance = null;

    private FactureManager() {
        super();
    }

    public static synchronized FactureManager getInstance() {
        if (instance == null)
            instance = new FactureManager();
        return instance;
    }

    @Override
    public Facture create(Facture f) {

        //On ajoute en base l'objet

        trx.begin();
        session.save(f);
        trx.commit();

        return f;
    }

    @Override
    public void delete(Facture f) {
        em.remove(f);
        em.flush();
        em.clear();
    }

    @Override
    public Facture update(Facture f) {
        trx.begin();
        session.update(f);
        trx.commit();

        return f;
    }

    @Override
    public Facture findById(int id) {
        return em.createQuery("select f from Facture f where f.id = :id", Facture.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<Facture> findAll() {
        return em.createQuery("from Facture", Facture.class)
                .getResultList();
    }

    @Override
    public List<Facture> findByName(String name) {
        return em.createQuery(
                "SELECT f " +
                        "FROM Facture f " +
                        "WHERE lower(f.designation) " +
                        "LIKE lower(:name) ", Facture.class)
                .setParameter("name", name +"%")
                .getResultList();
    }

    public List<Facture> findByDate(String date){
        return session.createQuery("from Facture f where f.dateFacture = :date ", Facture.class)
                .setParameter("date", date)
                .getResultList();
    }
}

