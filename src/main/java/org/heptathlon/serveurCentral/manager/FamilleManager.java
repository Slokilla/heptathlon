package org.heptathlon.serveurCentral.manager;


import org.heptathlon.serveurCentral.entity.Famille;

import java.util.ArrayList;
import java.util.List;

public class FamilleManager extends TableOperations<Famille> {

    private static FamilleManager instance = null;

    private FamilleManager() {
        super();
    }

    public static synchronized FamilleManager getInstance() {
        if (instance == null)
            instance = new FamilleManager();
        return instance;
    }

    @Override
    public Famille create(Famille f) {

        //On ajoute en base l'objet

        trx.begin();
        session.save(f);
        trx.commit();

        return f;
    }

    @Override
    public void delete(Famille f) {
        em.remove(f);
        em.flush();
        em.clear();
    }

    @Override
    public Famille update(Famille f) {
        trx.begin();
        session.update(f);
        trx.commit();

        return f;
    }

    @Override
    public Famille findById(int id) {
        return em.createQuery("select f from Famille f where f.id = :id", Famille.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<Famille> findAll() {
        return (ArrayList<Famille>)em.createQuery("from Famille", Famille.class)
                .getResultList();
    }

    @Override
    public List<Famille> findByName(String name) {
        return em.createQuery(
                "SELECT f " +
                        "FROM Famille f " +
                        "WHERE lower(f.nom) " +
                        "LIKE lower(:name) ", Famille.class)
                .setParameter("name", name +"%")
                .getResultList();
    }

}
