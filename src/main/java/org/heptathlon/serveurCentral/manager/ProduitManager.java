package org.heptathlon.serveurCentral.manager;


import org.heptathlon.serveurCentral.entity.Famille;
import org.heptathlon.serveurCentral.entity.Magasins_Produits;
import org.heptathlon.serveurCentral.entity.Produits;

import java.util.List;

public class ProduitManager extends TableOperations<Produits> {

    private static ProduitManager instance = null;

    public static synchronized ProduitManager getInstance() {
        if (instance == null)
            instance = new ProduitManager();
        return instance;
    }

    private ProduitManager() {
        super();
    }

    @Override
    public Produits create(Produits p) {
        trx.begin();
        session.save(p);
        trx.commit();

        return p;
    }

    @Override
    public void delete(Produits p) {
        em.remove(p);
        em.flush();
        em.clear();
    }

    @Override
    public Produits update(Produits p) {
        trx.begin();
        session.update(p);
        trx.commit();

        return p;
    }

    @Override
    public Produits findById(final int id) {
        return session.createQuery("select p from Produits p where p.id = :id", Produits.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<Produits> findAll() {
        return session.createQuery("from Produits ", Produits.class)
                .getResultList();
    }

    @Override
    public List<Produits> findByName(final String name) {
        return session.createQuery(
                "SELECT p " +
                        "FROM Produits p " +
                        "WHERE lower(p.reference) " +
                        "LIKE lower(:name) ", Produits.class)
                .setParameter("name", name +"%")
                .getResultList();
    }

    /**
     * Consulter le stock d'un article dans un magasin.
     *
     * @param id l'identifiant du Produits'
     * @return la quantité en stock du Produits specifié
     */
    public int countById(final int id){
        Magasins_Produits stock = session.createQuery("" +
                "select p " +
                "from Produits p " +
                "where p.id = :id_prod ", Magasins_Produits.class)
                .setParameter("id_prod", id)
                .getSingleResult();

        return stock.getQuantite();
    }


    /**
     * Récupérer la liste des articles d'une famille de Produitss.
     *
     * @param id l'id de la famille d'articles
     * @return une liste des articles correspondants a la famille
     */
    public List<Produits> findByFamille(final int id){
        return session.createQuery("from Produits " +
                        "         where famille.id = :id", Produits.class)
                                  .setParameter("id", id)
                                  .getResultList();
    }

    /**
     * Récupérer la liste des articles d'une famille de Produitss.
     *
     * @param f la famille d'articles
     * @return une liste des articles correspondants a la famille
     */
    public List<Produits> findByFamille(final Famille f){
        return session.createQuery("from Produits " +
                        "         where famille.id = :id", Produits.class)
                                  .setParameter("id", f.getId())
                                  .getResultList();
    }

    /**
     * Récupérer la liste des articles d'une famille de Produitss
     * à partir d'une recherche sur le nom.
     *
     * @param s la chaine à chercher
     * @return une liste des articles correspondants au résultat de la recherche
     */
    public List<Produits> findByFamille(final String s){
        return session.createQuery("from Produits " +
                        "         where lower(famille.nom) LIKE lower(:name)", Produits.class)
                                  .setParameter("name", s+"%")
                                  .getResultList();
    }

    /**
     * Méthode qui retourne le stock d'un Produits
     * @param id l'id du Produits dont on consult le stock
     * @return le stock
     */
    public Magasins_Produits findStock(final int id, final int id_mag){
        return session.createQuery("" +
                "select mp " +
                "from Magasins_Produits mp " +
                "where mp.produitsId = :id_prod " +
                "and mp.magasinsId = :id_mag", Magasins_Produits.class)
                .setParameter("id_prod", id)
                .setParameter("id_mag", id_mag)
                .getSingleResult();
    }

    public void saveStock(final Magasins_Produits mp){
        session.beginTransaction();
        session.saveOrUpdate(mp);
        session.getTransaction().commit();
    };
}

