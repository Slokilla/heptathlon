package org.heptathlon.serveurCentral.manager;

import org.heptathlon.serveurCentral.factory.sessionFactory;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class TableOperations<T> {

    protected Session session;
    protected final Metamodel metamodel;
    protected static EntityManager em;
    Transaction trx;

    public TableOperations() {
        session = sessionFactory.getSession();
        this.metamodel = session.getSessionFactory().getMetamodel();
        em = session.getSessionFactory().createEntityManager();
        trx = session.getTransaction();
    }


    public abstract T create(T obj);
    public abstract void delete(T obj);
    public abstract T update(T obj);
    public abstract T findById(int id);
    public abstract List<T> findAll();
    public abstract List<T> findByName(String name);

}
