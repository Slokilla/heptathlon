package org.heptathlon.serveurCentral.operation;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CentralService extends Remote {


    String LOOKUPNAME = "ServerCentral";

    void envoyerFactures(String factures) throws RemoteException;

}
