package org.heptathlon.serveurCentral.operation;

import org.heptathlon.serveurCentral.factory.sessionFactory;
import org.heptathlon.serveurCentral.manager.FactureManager;
import org.heptathlon.serveurCentral.manager.FamilleManager;
import org.heptathlon.serveurCentral.manager.ProduitManager;
import org.hibernate.Metamodel;
import org.hibernate.Session;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Utilisation : Operation.getInstance().
 */
public final class Operations extends UnicastRemoteObject implements CentralService {
    /**
     * instance permettant de créer un singleton.
     */
    private static volatile Operations instance = null;
    final Metamodel metamodel;
    /**
     * Session permettant l'acces a la base.
     */
    Session session;
    FamilleManager familleManager;
    ProduitManager produitManager;
    FactureManager factureManager;

    /**
     * Constructeur de l'objet.
     */
    private Operations() throws RemoteException {
        super();
        session = sessionFactory.getSession();
        metamodel = session.getSessionFactory().getMetamodel();
        factureManager = FactureManager.getInstance();
        familleManager = FamilleManager.getInstance();
        produitManager = ProduitManager.getInstance();
    }

    /**
     * Méthode permettant de renvoyer une instance de la classe Operations.
     *
     * @return Retourne l'instance du singleton.
     */
    public static synchronized Operations getInstance() throws RemoteException {
        if (Operations.instance == null) {
            Operations.instance = new Operations();
        }
        return Operations.instance;
    }

    @Override
    public void envoyerFactures(String factures) throws RemoteException {

    }


}
