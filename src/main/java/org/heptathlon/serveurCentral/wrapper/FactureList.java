package org.heptathlon.serveurCentral.wrapper;


import org.heptathlon.serveurCentral.entity.Facture;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement()
@XmlSeeAlso(Facture.class)
public class FactureList {

    @XmlElement(name = "Famille")
    private List<Facture> list;

    public FactureList() {
    }

    public FactureList(List<Facture> list) {
        this.list = list;
    }

    public List<Facture> getList() {
        return list;
    }
}

