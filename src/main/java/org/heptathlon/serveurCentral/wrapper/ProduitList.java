package org.heptathlon.serveurCentral.wrapper;

import org.heptathlon.serveurCentral.entity.Produits;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement()
@XmlSeeAlso(Produits.class)
public class ProduitList {

    @XmlElement(name = "Produit")
    private List<Produits> list;

    public ProduitList(){}

    public ProduitList(List<Produits> list){
        this.list=list;
    }

    public List<Produits> getList(){
        return list;
    }
}

